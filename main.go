package main

import (
	"database/sql"
	"encoding/json"
	_ "encoding/json"
	"fmt"
	"log"
	"net/http"

	_ "github.com/lib/pq"
)

var db *sql.DB

type Good struct {
	Id          int
	Name        string
	Description string
	Price       int
	Count       int
}

type Goods struct {
	Goods []Good
}

func main() {
	var err error

	db, err = sql.Open("postgres", "host=127.0.0.1 user=api password=123456 dbname=api sslmode=disable")
	if err != nil {
		panic(err)
	}

	defer db.Close()

	fmt.Printf("# app started...\n")
	http.HandleFunc("/v1/goods/", GetGoods)
	http.HandleFunc("/v1/goods/add", AddGood)
	log.Fatal(http.ListenAndServe(":8082", nil))

}

/**
@api {get} /goods/ Request all goods information
@apiPermission none
@apiVersion 0.1.0
@apiName GetGoods
@apiGroup Goods

@apiSuccess {Number} id Goods unique ID.
@apiSuccess {String} name   Name of the Goods.
@apiSuccess {String} description  Description of the Goods
@apiSuccess {Number} Price  Price of the Goods.
@apiSuccess {Number} count  Count of the Goods.

@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
    {
	    "Goods": [
    	    {
        	    "Id": 4,
            	"Name": "milk",
				"Description": "milk from cow",
				"Price": 1
				"Count": 120,
        	},
        	{
            	"Id": 3,
            	"Name": "pork",
				"Description": "this is meat",
				"Price": 45
				"Count": 1000,
        	},
        	{
            	"Id": 2,
            	"Name": "duck",
            	"Description": "this is meat",
            	"Price": 14
				"Count": 200,
        	}
    	]
	}

@apiError MethodNotAllowed  You must use only the method "GET"
@apiErrorExample Error-Response:
	 HTTP/1.1 405 Method Not Allowed
**/

func GetGoods(w http.ResponseWriter, r *http.Request) {

	if r.Method != "GET" {
		http.Error(w, "Method Not Allowed", 405)
	} else {

		w_array := Goods{}

		fmt.Println("#quering")

		rows, err := db.Query("SELECT id, name, description, price, count from goods;")
		if err != nil {
			panic(err)
		}

		for rows.Next() {
			w_good := Good{}

			err = rows.Scan(&w_good.Id, &w_good.Name, &w_good.Description, &w_good.Price, &w_good.Count)
			if err != nil {
				panic(err)
			}
			w_array.Goods = append(w_array.Goods, w_good)
		}

		json.NewEncoder(w).Encode(w_array)
	}
}

/**
@api {get} /goods/:id Request a goods information
@apiExample {curl} Example usage:
	curl -i http://127.0.0.1/v1/goods/3
@apiPermission none
@apiVersion 0.1.0
@apiName GetGood
@apiGroup Goods

@apiParam {Number} id Goods unique ID.

@apiSuccess {Number} id Goods unique ID.
@apiSuccess {String} name   Name of the Goods.
@apiSuccess {String} description  Description of the Goods
@apiSuccess {Number} Price  Price of the Goods.
@apiSuccess {Number} count  Count of the Goods.
@apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
       	{
           	"Id": 3,
           	"Name": "pork",
			"Description": "this is meat",
			"Price": 45
			"Count": 1000,
        }

@apiError GoodsNotFound The id of the Goods was not found.
@apiErrorExample Error-Response:
    HTTP/1.1 404 Not Found
    {
      "error": "GoodsNotFound"
    }

@apiError MethodNotAllowed  You must use only the method "GET"
@apiErrorExample Error-Response:
     HTTP/1.1 405 Method Not Allowed
**/

/**
@api {post} /goods/add/ Add a new goods
@apiExample {curl} Example usage:
	curl  -X POST  -H "Authorization: Bearer :token" http://127.0.0.1/v1/goods/add -d  '{"Name":"test","Description":"flour","Count":20,"Price":100}'
@apiPermission admin
@apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with supplied Auth Token
@apiVersion 0.1.0
@apiName PostGoods
@apiGroup Goods

@apiParam {String} name   Name of the Goods.
@apiParam {String} description  Description of the Goods
@apiParam {Number} Price  Price of the Goods.
@apiParam {Number} count  Count of the Goods.

@apiSuccess {Number} id Goods unique ID.


 @apiSuccessExample Success-Response:
    HTTP/1.1 201 OK
        {Id": 10}

@apiError MethodNotAllowed  You must use only the method "POST"
@apiErrorExample Error-Response:
     HTTP/1.1 405 Method Not Allowed
**/

func AddGood(w http.ResponseWriter, r *http.Request) {

	if r.Method != "POST" {
		http.Error(w, "Method Not Allowed", 405)
	} else {

		decoder := json.NewDecoder(r.Body)
		var g_good Good
		err := decoder.Decode(&g_good)
		if err != nil {
			panic(err)
		}

		query := fmt.Sprintf("INSERT INTO goods(name, description, price, count) VALUES('%s', '%s', %d, %d) RETURNING id;", g_good.Name, g_good.Description, g_good.Price, g_good.Count)

		rows, err := db.Query(query)
		if err != nil {
			panic(err)
		}

		for rows.Next() {
			var id int

			err = rows.Scan(&id)
			if err != nil {
				panic(err)
			}
			fmt.Fprintf(w, "{\"id\":%d}", id)
		}

		fmt.Println("#Insert Query: ", query)

	}
}

/**
@api {put} /goods/:id Change a goods
@apiExample {curl} Example usage:
	curl -i -X PUT  -H "Authorization: Bearer :token" http://127.0.0.1/v1/goods/3
@apiPermission admin
@apiHeader {String="Bearer :token"} Authorization Replace <code>:token</code> with supplied Auth Token
@apiVersion 0.1.0
@apiName PutGoods
@apiGroup Goods

@apiParam {String} name   Name of the Goods.
@apiParam {String} description  Description of the Goods
@apiParam {Number} Price  Price of the Goods.
@apiParam {Number} count  Count of the Goods.

@apiSuccess {Number} id Goods unique ID.
@apiSuccess {String} name   Name of the Goods.
@apiSuccess {String} description  Description of the Goods
@apiSuccess {Number} Price  Price of the Goods.
@apiSuccess {Number} count  Count of the Goods.


 @apiSuccessExample Success-Response:
    HTTP/1.1 200 OK
        {
      	    "Id": 4,
           	"Name": "milk",
			"Description": "milk from cow",
			"Price": 1
			"Count": 120,
       	}

@apiError MethodNotAllowed  You must use only the method "PUT"
@apiErrorExample Error-Response:
    HTTP/1.1 405 Method Not Allowed
**/
